FROM phusion/baseimage:latest

MAINTAINER Alex Berg

## Build variables (use ENV instead?)
ARG PSC_RELEASE_NAME=v0.10.3
ARG PULP_RELEASE_NAME=10.0.0
ARG PSC_RELEASE_TARFILE_NAME=linux64
## The SHA file expects the tar file to be inside a bundle directory.
##   This is only needed to simplify sha1sum.
ARG PSC_RELEASE_TARFILE_PATH=bundle/
## (Is making unprivileged user necessary?)
ARG USER_NAME=team

## Internal-only variables
ARG TEMP_DIR_PSC_RELEASE=temp-purescript-release

RUN useradd -m -s /bin/bash $USER_NAME

## Set up project directory, install PureScript.
RUN mkdir /home/$USER_NAME/bin \
    && mkdir /home/$USER_NAME/workspace \
    && chown $USER_NAME /home/$USER_NAME/workspace \
    && curl -L -O https://github.com/purescript/purescript/releases/download/$PSC_RELEASE_NAME/$PSC_RELEASE_TARFILE_NAME.tar.gz \
    && curl -L -O https://github.com/purescript/purescript/releases/download/$PSC_RELEASE_NAME/$PSC_RELEASE_TARFILE_NAME.sha \
    && mkdir $PSC_RELEASE_TARFILE_PATH \
    && mv $PSC_RELEASE_TARFILE_NAME.tar.gz $PSC_RELEASE_TARFILE_PATH \
    && ls && ls $PSC_RELEASE_TARFILE_PATH \
    && sha1sum -c "$PSC_RELEASE_TARFILE_NAME.sha" \
    && mkdir $TEMP_DIR_PSC_RELEASE \
    && tar -xvf $PSC_RELEASE_TARFILE_PATH$PSC_RELEASE_TARFILE_NAME.tar.gz -C $TEMP_DIR_PSC_RELEASE --strip-components=1 \
    && rm -rf $PSC_RELEASE_TARFILE_PATH \
    && ls $TEMP_DIR_PSC_RELEASE/psc* \
    && mv $TEMP_DIR_PSC_RELEASE/psc* /home/$USER_NAME/bin/ \
    && rm -rf $TEMP_DIR_PSC_RELEASE \
    && rm $PSC_RELEASE_TARFILE_NAME.sha

## Install NodeJS 4, Git, Bower, Pulp.
## Would like to remove these web dev things. Consider moving to separate Dockerfile.
RUN curl -sL https://deb.nodesource.com/setup_4.x | bash - \
    && apt-get update \
    && apt-get -y install git nodejs build-essential \
    && apt-get clean \
    && npm install -g bower pulp@$PULP_RELEASE_NAME \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV PATH /home/$USER_NAME/bin:$PATH

# Login here as this user when `docker run`
WORKDIR /home/$USER_NAME/workspace
USER $USER_NAME
