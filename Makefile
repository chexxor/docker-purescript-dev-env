.PHONY: image publish

IMAGE_NAME := purescript-dev-env

image:
	docker build -t="chexxor/$(IMAGE_NAME)" -f="./Dockerfile" .

enter:
	docker run -it --rm chexxor/$(IMAGE_NAME) bash

publish:
	docker push chexxor/$(IMAGE_NAME)
